# Introduccion a la programacion en Python 3.

<img src="/banner-Python.jpeg" style="width:400px;">


# Descripcion del curso:

Introduccion a la programacion en python 3 es un curso con una duracion de 20 horas, subdivididas en cinco clases de 4 horas, en las que se tocan temas como:

-String<br/>
-Listas<br/>
-numpy<br/>
-Funciones<br/>
-Diccionarios<br/>